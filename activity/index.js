/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function detailsOne(){
		let promptOne = prompt("Enter Full Name? "); 
		let promptTwo = prompt("How Old are you? ");
		let promptThree = prompt("Where do you live? "); 


		console.log("Hello, " + promptOne);
		console.log("You are " + promptTwo + " years old");
		console.log("You live in " + promptThree);
	}


	detailsOne();
    

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function topFive(){
		
		console.log("1. J Cole");
		console.log("2. Kendrick");
		console.log("3. 2pac");
		console.log("4. Nas");
		console.log("5. Eminem");
	}

	topFive();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function topMovies(){
		console.log("1. The Godfather");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("2. The Godfather, Part II");
		console.log("Rotten Tomatoes Rating: 96%");
		console.log("3. Shawshank Redemption");
		console.log("Rotten Tomatoes Rating: 91%");
		console.log("4. To Kill A Mockingbird");
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("5. Psycho");
		console.log("Rotten Tomatoes Rating: 96%");
	}


	topMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();
